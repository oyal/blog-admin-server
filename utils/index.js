module.exports = {
  success(data, message, code=200) {
    return {
      code,
      data,
      message
    }
  },
  fail(message, code=400) {
    return {
      code,
      message
    }
  }
}
