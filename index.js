const Koa = require('koa')
const app = new Koa()
const routers = require('./routers.js')
const bodyparser = require('koa-bodyparser')
const jwt = require('koa-jwt')
const { fail } = require('./utils/index.js')
const cors = require('koa2-cors')

require('./config/db.js')

app.use(cors())
app.use(bodyparser())

app.use((ctx, next) => {
  return next().catch(err => {
    if (err.status === 401) {
      ctx.body = fail(err.originalError ? err.originalError.message : err.message, 401)
    } else {
      throw err
    }
  })
})
app.use(jwt({ secret: 'oyal' }).unless({
  path: [/^\/api\/user\/register/, /^\/api\/user\/login/]
}))

app.use(routers.routes())

app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
})

app.listen(3001, () => {
  console.log('http://localhost:3001')
})
