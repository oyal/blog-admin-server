const router = require('koa-router')()
const users = require('./routes/users.js')
const roles = require('./routes/roles.js')
const permission = require('./routes/permission.js')
const articles = require('./routes/articles.js')

router.prefix('/api')

router.use(users.routes(), users.allowedMethods())
router.use(roles.routes(), roles.allowedMethods())
router.use(permission.routes(), permission.allowedMethods())
router.use(articles.routes(), articles.allowedMethods())

module.exports = router
