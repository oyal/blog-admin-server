const router = require('koa-router')()
const Role = require('../models/Role.js')
const { success, fail } = require('../utils/index.js')
const { isEn } = require('../utils/i18n.js')
const enRoleData = require('../doc/database/en/roles.json')
const zhRoleData = require('../doc/database/zh/roles.json')

router.prefix('/role')

router.get('/list', async ctx => {
  const roles = await Role.findAndCountAll({ raw: true })
  ctx.body = success(roles, '获取成功')
})

router.get('/permission', async ctx => {
  const { id } = ctx.query
  let roleData
  if (isEn(ctx)) {
    roleData = enRoleData
  } else {
    roleData = zhRoleData
  }
  const result = roleData.find(item => item.id == id)
  ctx.body = success(result.permissions, '获取成功')
})

router.post('/distribute-permission', async (ctx, next) => {
  let { roleId, permissions } = ctx.request.body
  // 不允许修改 超管 和 管理员权限
  if (roleId === '1' || roleId === '2') {
    return ctx.body = fail('不允许修改超管和管理员权限')
  }

  if (!permissions || !(permissions instanceof Array)) {
    return fail('修改失败')
  }

  let roleData
  if (isEn(ctx)) {
    roleData = enRoleData
  } else {
    roleData = zhRoleData
  }
  const role = roleData.find((item) => item.id == roleId)
  if (!role) {
    return fail('修改失败')
  }
  role.permissions = permissions
  ctx.body = success('', '修改成功')
})

module.exports = router
