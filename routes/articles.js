const router = require('koa-router')()
const { success, fail } = require('../utils/index.js')
const Article = require('../models/Article.js')

router.prefix('/article')

router.get('/list', async ctx => {
  const { page, size } = ctx.query
  const currentPage = Number(page) || 1
  const pageSize = Number(size) || 2
  const offset = (currentPage - 1) * pageSize

  const articles = await Article.findAndCountAll({
    limit: pageSize,
    offset
  })
  ctx.body = success({
    ...articles,
    page: currentPage,
    size: pageSize
  }, '获取成功')
})

router.post('/create', async ctx => {
  const article = await Article.create(ctx.request.body)
  ctx.body = success(article, '创建成功')
})

router.post('/sort', async ctx => {
  let { initRanking, finalRanking } = ctx.request.body
  // await Article.update({ ranking: finalRanking }, { where: { ranking: initRanking } })
  initRanking = parseInt(initRanking)
  finalRanking = parseInt(finalRanking)
  if (initRanking === finalRanking) {
    return ctx.body = fail('未更新')
  }
})

router.get('/detail', async ctx => {
  const { id } = ctx.query
  const article = await Article.findByPk(id)
  ctx.body = success(article, '查询成功')
})

router.post('/edit', async ctx => {
  const { id, title, content } = ctx.request.body
  console.log(id)
  const article = await Article.update({
    title,
    content
  }, {
    where: {
      id
    }
  })
  ctx.body = success(article, '修改成功')
})

router.post('/delete', async ctx => {
  const { id } = ctx.request.body
  console.log(id)
  const article = await Article.findByPk(id)
  if (!article) {
    return ctx.body = fail('删除失败')
  }
  await article.destroy()
  ctx.body = success('', '删除成功')
})

module.exports = router
