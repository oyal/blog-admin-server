const router = require('koa-router')()
const User = require('../models/User.js')
const Role = require('../models/Role.js')
const UR = require('../models/UserRole.js')
const jwt = require('jsonwebtoken')
const { success, fail } = require('../utils/index.js')
const { isEn } = require('../utils/i18n')
const enFeatureData = require('../doc/database/en/feature.json')
const zhFeatureData = require('../doc/database/zh/feature.json')
const enChapterData = require('../doc/database/en/chapter.json')
const zhChapterData = require('../doc/database/zh/chapter.json')
const enRoleData = require('../doc/database/en/roles.json')
const zhRoleData = require('../doc/database/zh/roles.json')
const enPermissionData = require('../doc/database/en/permission.json')
const zhPermissionData = require('../doc/database/zh/permission.json')

router.prefix('/user')

router.post('/register', async ctx => {
  const { roles } = ctx.request.body
  const roleList = []
  for (const role of roles) {
    const findRole = await Role.findOne({
      where: {
        name: role.name
      }
    })
    if (!findRole) {
      const newRole = await Role.create({ name: role.name })
      roleList.push(newRole)
    } else {
      roleList.push(findRole)
    }
  }
  const user = await User.create(ctx.request.body)
  await user.addRoles(roleList)
  ctx.body = success(user, '注册成功')
})

router.post('/login', async ctx => {
  const user = await User.findOne({
    where: ctx.request.body,
    raw: true
  })
  if (!user) {
    ctx.body = fail('用户名或密码错误')
  } else {
    const token = jwt.sign(user, 'oyal', { expiresIn: '7d' })
    ctx.body = success({ token }, '登陆成功')
  }
})

router.get('/profile', async ctx => {
  // const token = ctx.request.headers.authorization.split(' ').pop()
  const token = ctx.get('Authorization').split(' ').pop()
  // jwt.verify(token, 'oyal', (error, decode) => {
  //   if (error) return error
  //   console.log(decode)
  //   ctx.body = success({ userInfo: decode }, '获取成功')
  // })
  const decode = jwt.verify(token, 'oyal')
  const user = await User.findOne({
    where: {
      id: decode.id
    },
    include: [Role]
  })

  const permissionKeys = getUserPermissionKeys(ctx, user)
  const permissions = getPermission(ctx, permissionKeys)
  user.setDataValue('permissions', permissions)
  ctx.body = success({
    userInfo: user
  }, '获取成功')
})

function getUserPermissionKeys(ctx, user) {
  let roleData
  if (isEn(ctx)) {
    roleData = enRoleData
  } else {
    roleData = zhRoleData
  }
  const result = []
  user.roles.forEach((item) => {
    roleData.forEach((roleItem) => {
      if (item.id === roleItem.id) {
        result.push(...roleItem.permissions)
      }
    })
  })
  return result
}

function getPermission(ctx, keys) {
  let permissionData
  if (isEn(ctx)) {
    permissionData = enPermissionData
  } else {
    permissionData = zhPermissionData
  }
  const lateralPermission = getLateralPermission(permissionData)

  const menus = []
  const points = []
  keys.forEach((key) => {
    const permission = lateralPermission[key]
    if (key.includes('-')) {
      points.push(permission.permissionMark)
    } else {
      menus.push(permission.permissionMark)
    }
  })
  return {
    menus,
    points
  }
}

function getLateralPermission(permissionData) {
  let result = {}
  permissionData.forEach((item) => {
    result[item.id] = {
      id: item.id,
      permissionName: item.permissionName,
      permissionMark: item.permissionMark,
      permissionDesc: item.permissionDesc
    }
    if (item.children && item.children.length > 0) {
      result = {
        ...result,
        ...getLateralPermission(item.children)
      }
    }
  })
  return result
}

router.get('/list', async ctx => {
  const { page, size } = ctx.query
  const currentPage = Number(page) || 1
  const pageSize = Number(size) || 2
  const offset = (currentPage - 1) * pageSize

  const users = await User.findAndCountAll({
    include: [{ model: Role }],
    limit: pageSize,
    offset,
    attributes: {
      exclude: ['password']
    }
  })
  ctx.body = success({
    ...users,
    page: currentPage,
    size: pageSize
  }, '获取成功')
})

router.post('/batchImport', async ctx => {
  const data = await User.bulkCreate(ctx.request.body)
  ctx.body = success(data, '导入成功')
})

router.get('/detail', async ctx => {
  const user = await User.findOne({
    where: ctx.query,
    attributes: { exclude: ['password'] },
    include: [{ model: Role }]
  })
  ctx.body = success(user, '获取成功')
})

router.post('/delete', async ctx => {
  if (ctx.request.body.id === 1) {
    return ctx.body = fail('这个用户不能删除哦')
  }
  const result = await User.destroy({
    where: ctx.request.body
  })
  ctx.body = success(result, '删除成功')
})

router.post('/role', async ctx => {
  const { id, roles } = ctx.request.body
  if (id === '1') {
    return ctx.body = fail('超级管理员角色不允许修改')
  }
  const user = await User.findOne({ where: { id } })
  let newRoles = []
  for (const role of roles) {
    const newRole = await Role.findOne({ where: { name: role } })
    newRoles.push(newRole)
  }
  await user.setRoles(newRoles)
  ctx.body = success('', '修改成功')
})

router.get('/feature', ctx => {
  let data
  if (isEn(ctx)) {
    data = enFeatureData
  } else {
    data = zhFeatureData
  }
  ctx.body = success(data, '获取成功')
})

router.get('/chapter', ctx => {
  let data
  if (isEn(ctx)) {
    data = enChapterData
  } else {
    data = zhChapterData
  }
  ctx.body = success(data, '获取成功')
})

module.exports = router
