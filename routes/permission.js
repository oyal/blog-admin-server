const router = require('koa-router')()
const { success } = require('../utils/index.js')
const { isEn } = require('../utils/i18n.js')
const enPermissionData = require('../doc/database/en/permission.json')
const zhPermissionData = require('../doc/database/zh/permission.json')

router.prefix('/permission')

router.get('/list', async ctx => {
  let data
  if (isEn(ctx)) {
    data = enPermissionData
  } else {
    data = zhPermissionData
  }
  ctx.body = success(data, '获取成功')
})

module.exports = router
