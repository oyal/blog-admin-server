const { Sequelize } = require('sequelize')

const sequelize = new Sequelize('mysql://root@localhost:3306/blog-admin', { timezone: '+08:00' })

sequelize.authenticate().then(() => {
  console.log('Connection has been established successfully.')
}).catch(error => {
  console.error('Unable to connect to the database:', error)
})

// sequelize.sync({ force: true }).then(() => {
//   console.log('updated success')
// })


module.exports = sequelize
