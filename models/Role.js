const { Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../config/db.js')

const Role = sequelize.define('role', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  desc: DataTypes.TEXT,
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  }
})

// Role.sync({ alter: true }).then(() => {
//   console.log('alter success!')
// })

module.exports = Role
