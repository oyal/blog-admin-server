const { Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../config/db.js')

const Article = sequelize.define('article', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  content: DataTypes.TEXT,
  desc: DataTypes.TEXT,
  ranking: DataTypes.INTEGER,
  author: DataTypes.STRING,
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  }
})

// Article.sync({ alter: true }).then(() => {
//   console.log('alter success!')
// })

module.exports = Article
