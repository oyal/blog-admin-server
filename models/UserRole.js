const { Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../config/db.js')
const User = require('../models/User.js')
const Role = require('../models/Role.js')

const UR = sequelize.define('ur', {
  userId: {
    type: DataTypes.INTEGER,
    references: {
      model: User,
      key: 'id'
    }
  },
  roleId: {
    type: DataTypes.INTEGER,
    references: {
      model: Role,
      key: 'id'
    }
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  }
})

User.belongsToMany(Role, { through: UR })
Role.belongsToMany(User, { through: UR})

module.exports = UR
