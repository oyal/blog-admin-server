const { Sequelize, DataTypes } = require('sequelize')
const sequelize = require('../config/db.js')

const User = sequelize.define('user', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    default: 'e10adc3949ba59abbe56e057f20f883e'
  },
  avatar: DataTypes.STRING,
  nickname: DataTypes.STRING,
  mobile: DataTypes.STRING,
  gender: DataTypes.STRING,
  nationality: DataTypes.STRING,
  province: DataTypes.STRING,
  address: DataTypes.STRING,
  experience: DataTypes.STRING,
  major: DataTypes.STRING,
  glory: DataTypes.STRING,
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
  }
})

// User.sync({ alter: true }).then(() => {
//   console.log('alter success!')
// })

module.exports = User
